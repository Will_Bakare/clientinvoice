﻿using CustomerInvoiceZF.Repository;
using CustomerInvoiceZF.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoice.Service.Interfaces
{
    public interface ICustomerService
    {
        Task<CustomerResponse> GetCustomerByID(string id);
        Task<CustomerResponse> AddCustomerAsync(Customer customer);
        Task<BaseResponse> UpdateCustomerContactAsync(UpdateCustomerContactReq updateCustomerContactReq);
    }
}
