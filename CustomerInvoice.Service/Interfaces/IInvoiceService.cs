﻿using CustomerInvoiceZF.Repository;
using CustomerInvoiceZF.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoice.Service.Interfaces
{
   public interface IInvoiceService
    {
        Task<InvoiceResponse> GetInvoiceByIDAsync(string id);
        Task<InvoiceResponse> AddInvoiceAsync(InvoiceRequest invoice);
        Task<BaseResponse> UpdateInvoiceStatustAsync(UpdateInvoiceStatusReq updateInvoiceStatusReq);
    }
}
