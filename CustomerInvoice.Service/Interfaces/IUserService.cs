﻿using CustomerInvoiceZF.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoice.Service.Interfaces
{
    public interface IUserService
    {
        Task<UserModel> GetAsync(string username, string passkey);
        Task<UserModel> AddAsync(UserModel user);
    }
}
