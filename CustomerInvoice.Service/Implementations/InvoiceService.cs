﻿using CustomerInvoice.Service.Interfaces;
using CustomerInvoiceZF.Repository;
using CustomerInvoiceZF.Repository.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoice.Service.Implementations
{
    public class InvoiceService : IInvoiceService
    {

        private readonly IInvoiceDbService _invoiceDbService;
        private readonly ICustomerDbService _customerDbService;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public InvoiceService(IInvoiceDbService invoiceDbService , ICustomerDbService customerDbService)
        {
            _invoiceDbService = invoiceDbService;
            _customerDbService = customerDbService;
        }
        public async Task<InvoiceResponse> AddInvoiceAsync(InvoiceRequest invoiceRequest)
        {
            var requestParam = JsonConvert.SerializeObject(invoiceRequest);
            Logger.Info("AddInvoiceAsync.Request:" + requestParam);
            Invoice invoice = new Invoice();
            InvoiceResponse invoiceResponse = new InvoiceResponse();
            try
            {
                var customerDetails = await _customerDbService.GetAsync(invoiceRequest.CustomerID);
                if (customerDetails.ResponseCode == "00")
                {
                    invoice.Customer = customerDetails.CreatedCustomer;
                }
                else
                {
                    invoiceResponse = new InvoiceResponse();
                    invoiceResponse.ResponseCode = "60";
                    invoiceResponse.ResponseDescription = "Unable to fetch customer details";
                    return invoiceResponse;
                }

                invoice.id = Guid.NewGuid().ToString();
                decimal totalAmount = 0;
                foreach(var line in invoiceRequest.InvoiceLines)
                {
                    decimal amount = line.UnitPrice * line.Quantity;
                    totalAmount += amount;
                }
                invoice.TotalAmount = totalAmount;
                invoice.Date = DateTime.Now; ;
                invoice.Description = invoiceRequest.Description;
                invoice.InvoiceLines = invoiceRequest.InvoiceLines;
                invoice.Status = invoiceRequest.Status;
                invoiceResponse = await _invoiceDbService.AddAsync(invoice);
            }
            catch(Exception ex)
            {
                Logger.Error("AddInvoiceAsync Exception" + ex.Message + ".StackTrace:" + ex.StackTrace + ". Request:"+ requestParam);
                invoiceResponse.ResponseCode = "50";
                invoiceResponse.ResponseDescription = "An error occured while processing your request";
            }
            return invoiceResponse;
        }

        public async Task<InvoiceResponse> GetInvoiceByIDAsync(string id)
        {
            Logger.Info("GetInvoiceByIDAsync.Request:" + id);
            InvoiceResponse invoiceResponse = new InvoiceResponse();
            try
            {
                invoiceResponse = await _invoiceDbService.GetAsync(id);
            }
            catch (Exception ex)
            {
                Logger.Error("GetInvoiceByIDAsync Exception" + ex.Message + ".StackTrace:" + ex.StackTrace + ". Request:" + id);

                invoiceResponse.ResponseCode = "50";
                invoiceResponse.ResponseDescription = "An error occured while processing your request";
            }
            return invoiceResponse;
        }

        public async Task<BaseResponse> UpdateInvoiceStatustAsync(UpdateInvoiceStatusReq updateInvoiceStatusReq)
        {
            var requestParam = JsonConvert.SerializeObject(updateInvoiceStatusReq);
            Logger.Info("UpdateInvoiceStatustAsync.Request:" + requestParam);
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                var invoiceResponse = await _invoiceDbService.GetAsync(updateInvoiceStatusReq.id);
                if (invoiceResponse.ResponseCode == "00")
                {
                    Invoice invoice = new Invoice();
                    invoice.id = updateInvoiceStatusReq.id;
                    invoice.Date = invoiceResponse.CreatedInvoice.Date;
                    invoice.Description = invoiceResponse.CreatedInvoice.Description;
                    invoice.Customer = invoiceResponse.CreatedInvoice.Customer;
                    invoice.TotalAmount = invoiceResponse.CreatedInvoice.TotalAmount;
                    invoice.InvoiceLines = invoiceResponse.CreatedInvoice.InvoiceLines;
                    invoice.Status = updateInvoiceStatusReq.Status;
                    invoice.StatusModifiedDate = DateTime.Now;
                    await _invoiceDbService.UpdateAsync(updateInvoiceStatusReq.id, invoice);
                    baseResponse.ResponseCode = "00";
                    baseResponse.ResponseDescription = "Success";
                }
                else
                {
                    Logger.Error("UpdateInvoiceStatustAsync.Request:" + requestParam +".Error:" + "Unable to fetch invoice details");
                    baseResponse.ResponseCode = "40";
                    baseResponse.ResponseDescription = "Unable to fetch invoice details";
                }

            }
            catch (Exception ex)
            {
                Logger.Error("UpdateInvoiceStatustAsync Exception" + ex.Message + ".StackTrace:" + ex.StackTrace + ". Request:" + requestParam);

                baseResponse.ResponseCode = "50";
                baseResponse.ResponseDescription = "An error occured while processing your request.";
            }
            return baseResponse;
        }

    }
}
