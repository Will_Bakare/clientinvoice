﻿using CustomerInvoice.Service.Interfaces;
using CustomerInvoiceZF.Repository;
using CustomerInvoiceZF.Repository.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoice.Service.Implementations
{
    public class CustomerService : ICustomerService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ICustomerDbService _customerDbService;
        public CustomerService(ICustomerDbService customerDbService)
        {
            _customerDbService = customerDbService;
        }
        public async Task<CustomerResponse> AddCustomerAsync(Customer customer)
        {
            var requestParam = JsonConvert.SerializeObject(customer);
            Logger.Info("AddCustomerAsync.Request:" + requestParam);
            CustomerResponse createCustomerResponse = new CustomerResponse();
            try
            {
                customer.id = Guid.NewGuid().ToString();
                createCustomerResponse = await _customerDbService.AddAsync (customer);
            }
            catch(Exception ex)
            {
                Logger.Error("AddCustomerAsync Exception" + ex.Message + ".StackTrace:" + ex.StackTrace + ". Request:" + requestParam);

                createCustomerResponse.ResponseCode = "50";
                createCustomerResponse.ResponseDescription = "An error occured while processing your request";
            }

            return createCustomerResponse;
        }

        public async Task<CustomerResponse> GetCustomerByID(string id)
        {
            Logger.Info("GetCustomerByID.Request:" + id);
            CustomerResponse createCustomerResponse = new CustomerResponse();
            try
            {
                createCustomerResponse = await _customerDbService.GetAsync(id);
            }
            catch (Exception ex)
            {
                Logger.Error("GetCustomerByID Exception" + ex.Message + ".StackTrace:" + ex.StackTrace + ". Request:" + id);

                createCustomerResponse.ResponseCode = "50";
                createCustomerResponse.ResponseDescription = "An error occured while processing your request";
            }
            return createCustomerResponse;
        }

        public async Task<BaseResponse> UpdateCustomerContactAsync(UpdateCustomerContactReq updateCustomerContactReq)
        {
            var requestParam = JsonConvert.SerializeObject(updateCustomerContactReq);
            Logger.Info("UpdateCustomerContactAsync.Request:" + requestParam);
            BaseResponse baseResponse = new BaseResponse();
            try
            {

                var customerResponse = await _customerDbService.GetAsync(updateCustomerContactReq.id);
                if (customerResponse.ResponseCode == "00")
                {
                    Customer customer = new Customer();
                    customer.id = updateCustomerContactReq.id;
                    customer.FirstName = customerResponse.CreatedCustomer.FirstName;
                    customer.LastName = customerResponse.CreatedCustomer.LastName;
                    customer.Address = customerResponse.CreatedCustomer.Address;
                    customer.ContactEntries = updateCustomerContactReq.ContactDetails;
                    await _customerDbService.UpdateAsync(updateCustomerContactReq.id, customer);
                    baseResponse.ResponseCode = "00";
                    baseResponse.ResponseDescription = "Success";
                }
                else
                {
                    baseResponse.ResponseCode = "40";
                    baseResponse.ResponseDescription = "Unable to fetch customer details";
                }

            }
            catch (Exception ex)
            {
                Logger.Error("UpdateCustomerContactAsync Exception" + ex.Message + ".StackTrace:" + ex.StackTrace + ". Request:" + requestParam);

                baseResponse.ResponseCode = "50";
                baseResponse.ResponseDescription = "An error occured while processing your request.";
            }
            return baseResponse;


        }

    }
}
