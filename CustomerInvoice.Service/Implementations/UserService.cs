﻿using CustomerInvoice.Service.Interfaces;
using CustomerInvoiceZF.Repository.Interfaces;
using CustomerInvoiceZF.Repository.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoice.Service.Implementations
{
    public class UserService : IUserService
    {
        public IConfiguration _configuration;
        IUserDbService _userDbService;

        public UserService(IConfiguration config, IUserDbService userDbService)
        {
            _configuration = config;
            _userDbService = userDbService;
        }
        public async Task<UserModel> AddAsync(UserModel user)
        {
            UserModel userModel = new UserModel();
            try
            {
                user.id = Guid.NewGuid().ToString();
                userModel = await _userDbService.AddAsync(user);

            }
            catch
            {

            }
            return userModel;
        }

        public async Task<UserModel> GetAsync(string id, string passkey)
        {
            UserModel userModel = new UserModel();
            try
            {
                userModel = await _userDbService.GetAsync(id, passkey);
                if(userModel.Passkey == passkey)
                {
                    return userModel;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
           
        }
    }
}
