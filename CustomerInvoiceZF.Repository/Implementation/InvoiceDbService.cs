﻿using CustomerInvoiceZF.Repository.Models;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.Repository
{
    public class InvoiceDbService : IInvoiceDbService
    {

        private Container _container;

        public InvoiceDbService(
           CosmosClient cosmosDbClient,
           string databaseName,
           string containerName
            )
        {
            _container = cosmosDbClient.GetContainer(databaseName, containerName);
        }
        public async Task<InvoiceResponse> AddAsync(Invoice invoice)
        {
            InvoiceResponse invoiceResponse = new InvoiceResponse();
            try
            {
                var createInvoiceRes = await _container.CreateItemAsync(invoice, new PartitionKey(invoice.id));
                invoiceResponse.CreatedInvoice = createInvoiceRes.Resource;
                invoiceResponse.ResponseCode = "00";
                invoiceResponse.ResponseDescription = "Success";
            }
            catch
            {
                invoiceResponse.ResponseCode = "50";
                invoiceResponse.ResponseDescription = "An error occured while processing your request";
            }
            return invoiceResponse;
        }

        public async Task<InvoiceResponse> GetAsync(string id)
        {
            InvoiceResponse invoiceResponse = new InvoiceResponse();
            try
            {
                var response = await _container.ReadItemAsync<Invoice>(id, new PartitionKey(id));
                invoiceResponse.CreatedInvoice = response.Resource;
                invoiceResponse.ResponseCode = "00";
                invoiceResponse.ResponseDescription = "Success"; 
            }
            catch (Exception ex)
            {
                invoiceResponse.ResponseCode = "50";
                invoiceResponse.ResponseDescription = "An error occured while processing your request";
            }
            return invoiceResponse;
        }

        public async Task<IEnumerable<Invoice>> GetMultipleAsync(string queryString)
        {
            var query = _container.GetItemQueryIterator<Invoice>(new QueryDefinition(queryString));
            var results = new List<Invoice>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();
                results.AddRange(response.ToList());
            }
            return results;
        }

        public async Task UpdateAsync(string id, Invoice invoice)
        {
            await _container.UpsertItemAsync(invoice, new PartitionKey(id));
        }

          }
}
