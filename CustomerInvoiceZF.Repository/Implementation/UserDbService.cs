﻿using CustomerInvoiceZF.Repository.Interfaces;
using CustomerInvoiceZF.Repository.Models;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.Repository.Implementation
{
    public class UserDbService : IUserDbService
    {
        private Container _container;

        public UserDbService(
           CosmosClient cosmosDbClient,
           string databaseName,
           string containerName
            )
        {
            _container = cosmosDbClient.GetContainer(databaseName, containerName);
        }
        public async Task<UserModel> AddAsync(UserModel user)
        {
            UserModel userModel = new UserModel();
            
                var createCustomerRes = await _container.CreateItemAsync(user, new PartitionKey(user.id));
                userModel = createCustomerRes.Resource;
               
           
            return userModel;
        }

        public async Task<UserModel> GetAsync(string id, string passkey)
        {
            UserModel userModel = new UserModel();
            var response = await _container.ReadItemAsync<UserModel>(id, new PartitionKey(id));
            userModel = response.Resource;
            
            return userModel;
        }
    }
}
