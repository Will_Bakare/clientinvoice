﻿using CustomerInvoiceZF.Repository.Models;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.Repository
{
    public class CustomerDbService : ICustomerDbService
    {

        private Container _container;

        public CustomerDbService(
           CosmosClient cosmosDbClient,
           string databaseName,
           string containerName
            )
        {
            _container = cosmosDbClient.GetContainer(databaseName, containerName);
        }
        public async Task<CustomerResponse> AddAsync(Customer customer)
        {
            CustomerResponse createCustomerResponse = new CustomerResponse();
            try
            {
              
                var createCustomerRes =   await _container.CreateItemAsync(customer, new PartitionKey(customer.id));
                createCustomerResponse.CreatedCustomer = createCustomerRes.Resource;
                createCustomerResponse.ResponseCode = "00";
                createCustomerResponse.ResponseDescription = "Success";
            }
            catch
            {
                createCustomerResponse.ResponseCode = "50";
                createCustomerResponse.ResponseDescription = "An error occured while processing your request";
            }

            return createCustomerResponse;
        }

        public async Task<CustomerResponse> GetAsync(string id)
        {
            CustomerResponse createCustomerResponse = new CustomerResponse();
            try
            {
                var response = await _container.ReadItemAsync<Customer>(id, new PartitionKey(id));
                createCustomerResponse.CreatedCustomer =  response.Resource;
                createCustomerResponse.ResponseCode = "00";
                createCustomerResponse.ResponseDescription = "Success";
            }
            catch (Exception ex)
            {
                createCustomerResponse.ResponseCode = "50";
                createCustomerResponse.ResponseDescription = "An error occured while processing your request";
            }
            return createCustomerResponse;
        }

        public async Task<IEnumerable<Customer>> GetMultipleAsync(string queryString)
        {
            var query = _container.GetItemQueryIterator<Customer>(new QueryDefinition(queryString));
            var results = new List<Customer>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();
                results.AddRange(response.ToList());
            }
            return results;
        }

        public async Task UpdateAsync(string id, Customer customer)
        {

            await _container.UpsertItemAsync(customer, new PartitionKey(id));
        }

        

    }
}
