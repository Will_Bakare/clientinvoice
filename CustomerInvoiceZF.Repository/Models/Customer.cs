﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInvoiceZF.Repository
{
    public class Customer
    {
        public string id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public List<Contact> ContactEntries { get; set; }
    }

    public class Contact
    {
        public string Type { get; set; }
        public string Value { get; set; }
       
    }


}
