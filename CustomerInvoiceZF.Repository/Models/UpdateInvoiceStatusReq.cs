﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInvoiceZF.Repository
{
    public class UpdateInvoiceStatusReq
    {
        public string id { get; set; }
        public string Status { get; set; }
    }
}
