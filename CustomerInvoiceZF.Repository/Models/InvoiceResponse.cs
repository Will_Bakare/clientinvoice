﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInvoiceZF.Repository.Models
{
    public class InvoiceResponse:BaseResponse
    {
        public Invoice CreatedInvoice { get; set; }

    }
}
