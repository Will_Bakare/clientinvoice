﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInvoiceZF.Repository.Models
{
    public class CustomerResponse:BaseResponse
    {
        public Customer CreatedCustomer { get; set; }
    }
}
