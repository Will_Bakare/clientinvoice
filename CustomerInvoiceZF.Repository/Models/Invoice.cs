﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace CustomerInvoiceZF.Repository
{
    public class Invoice
    {
        public string id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public Customer Customer { get; set; }
        public decimal TotalAmount { get; set; }
        public string Status { get; set; }
        public DateTime StatusModifiedDate { get; set; }
        public List<InvoiceLine> InvoiceLines { get; set; }
    }

    public class InvoiceLine
    {
      //[JsonPropertyName("Quantity")]
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal LineAmount { get; set; }

    }

    public class InvoiceRequest
    {
        public string id { get; set; }
        public string Description { get; set; }
        public string CustomerID { get; set; }
        public string Status { get; set; }
        public List<InvoiceLine> InvoiceLines { get; set; }
    }
}
