﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInvoiceZF.Repository
{
    public class UpdateCustomerContactReq
    {
        public string id { get; set; }

        public List<Contact> ContactDetails { get; set; }
    }
}
