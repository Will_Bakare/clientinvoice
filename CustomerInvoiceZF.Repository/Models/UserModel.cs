﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInvoiceZF.Repository.Models
{
    public class UserModel
    {
        public string id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Passkey { get; set; }
    }
}
