﻿using CustomerInvoiceZF.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.Repository
{
   public  interface IInvoiceDbService
    {
        Task<IEnumerable<Invoice>> GetMultipleAsync(string query);
        Task<InvoiceResponse> GetAsync(string id);
        Task<InvoiceResponse> AddAsync(Invoice invoice);
        Task UpdateAsync(string id, Invoice invoice);
    }
}
