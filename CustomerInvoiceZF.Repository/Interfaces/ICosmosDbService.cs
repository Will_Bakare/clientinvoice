﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.Repository
{
   public interface ICosmosDbService
    {
        Task<IEnumerable<Item>> GetMultipleAsync(string query);
        Task<Item> GetAsync(string id);
        Task AddAsync(Item item);
        Task UpdateAsync(string id, Item item);
        Task DeleteAsync(string id);
    }
}
