﻿using CustomerInvoiceZF.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.Repository
{
    public interface ICustomerDbService
    {
        Task<IEnumerable<Customer>> GetMultipleAsync(string query);
        Task<CustomerResponse> GetAsync(string id);
        Task<CustomerResponse> AddAsync(Customer customer);
        Task UpdateAsync(string id, Customer customer);

    }
}
