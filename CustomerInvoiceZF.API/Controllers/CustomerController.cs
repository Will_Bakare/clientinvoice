﻿using CustomerInvoice.Service.Interfaces;
using CustomerInvoiceZF.Repository;
using CustomerInvoiceZF.Repository.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService ?? throw new ArgumentNullException(nameof(customerService));
        }

        [HttpPost]
        [Route("CreateCustomer")]
        public async Task<CustomerResponse> CreateCustomer([FromBody] Customer customer)
        {

            return await _customerService.AddCustomerAsync(customer);
        }



        [HttpGet]
        [Route("GetCustomerByID")]
        public async Task<CustomerResponse> GetCustomerByID(string id)
        {
            return await _customerService.GetCustomerByID(id);
           
        }

        
       
        [HttpPut]
        [Route("UpdateCustomerContactAsync")]
        public async Task<BaseResponse> UpdateCustomerContactAsync(UpdateCustomerContactReq updateCustomerContactReq)
        {
            return await _customerService.UpdateCustomerContactAsync(updateCustomerContactReq);
            
        }



    }
}
