﻿using CustomerInvoice.Service.Interfaces;
using CustomerInvoiceZF.Repository;
using CustomerInvoiceZF.Repository.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly IInvoiceService _invoiceService;
        public InvoiceController(IInvoiceService invoiceDbService)
        {
            _invoiceService = invoiceDbService;
        }

        [HttpPost]
        [Route("CreateInvoice")]
        public async Task<InvoiceResponse> CreateInvoice([FromBody] InvoiceRequest invoice)
        {

            return await _invoiceService.AddInvoiceAsync(invoice);
        }

        [HttpGet]
        [Route("GetInvoiceByID")]
        public async Task<InvoiceResponse> GetInvoiceByID(string id)
        {
            return await _invoiceService.GetInvoiceByIDAsync(id);
        }

        [HttpPut]
        [Route("UpdateInvoiceStatus")]
        public async Task<BaseResponse> UpdateInvoiceStatus(UpdateInvoiceStatusReq updateInvoiceStatusReq)
        {
            return await _invoiceService.UpdateInvoiceStatustAsync(updateInvoiceStatusReq);
        }


    }
}
