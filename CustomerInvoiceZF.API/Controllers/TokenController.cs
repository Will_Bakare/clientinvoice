﻿using CustomerInvoice.Service.Interfaces;
using CustomerInvoiceZF.Repository.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInvoiceZF.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        public IConfiguration _configuration;
        IUserService _userService;

        public TokenController(IConfiguration config, IUserService userService)
        {
            _configuration = config;
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Post(UserModel userModel)
        {
            try
            {
                if (userModel != null && userModel.id != null && userModel.Passkey != null)
                {
                    var user = await GetUser(userModel.id, userModel.Passkey);

                    if (user != null)
                    {
                        //create claims details based on the user information
                        var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim("Id", userModel.id),
                    new Claim("UserName", userModel.Username),
                    new Claim("Email", userModel.Email),
                    new Claim("Passkey", userModel.Passkey)

                   };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                        var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                        return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                    }
                    else
                    {
                        return BadRequest("Invalid credentials");
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception ex)
            {
                return BadRequest("Invalid credentials");
            }
          
        }

        private async Task<UserModel> GetUser(string id,string passkey)
        {
            return await _userService.GetAsync(id, passkey);
        }

        //[HttpPost]
        //[Route("CreateUser")]
        //public async Task<UserModel> CreateUser(UserModel userModel)
        //{
        //    return await _userService.AddAsync(userModel);
        //}
    }
}
